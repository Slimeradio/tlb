using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces.Terminal;
using Scripts.Shared;
using SharedLib;
using VRage;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;

namespace TLB
{
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_Refinery), true)]
    public class AbstractTimeLimitedRefineryBlock : TimeLimitedBlock
    {
        public override bool HasItemsToProcess()
        {
            var rref = (IMyRefinery)m_block;
            if (rref.InputInventory.CurrentVolume <= (MyFixedPoint)0.1)
            {
                return false;
            }

            if ((double)rref.OutputInventory.CurrentVolume >= (double)rref.OutputInventory.MaxVolume * 0.95d)
            {
                return false;
            }

            return true;
        }

        protected override void ApplyBoostMultipler(float v)
        {
            var reff = (IMyRefinery)m_block;
            reff.UpgradeValues["Boost"] = v;
        }
    }

    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_Assembler), true)]
    public class AbstractTimeLimitedAssemblerBlock : TimeLimitedBlock
    {
        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            base.Init(objectBuilder);
            try
            {
                var block = (Entity as IMyProductionBlock);
                block.AddUpgradeValue("Boost", 0);
            } catch (Exception e) {
                MyVisualScriptLogicProvider.SendChatMessage("Exception init " + e.ToString());
            }
        }

        public override bool HasItemsToProcess()
        {
            var assembler = (IMyAssembler)m_block;
            if (assembler.IsQueueEmpty)
            {
                return false;
            }

            if ((double)assembler.OutputInventory.CurrentVolume >= (double)assembler.OutputInventory.MaxVolume * 0.95d)
            {
                return false;
            }

            return true;
        }

        protected override void ApplyBoostMultipler(float v)
        {
            var assembler = (IMyAssembler)m_block;
            assembler.UpgradeValues["Boost"] = v;
        }
    }

    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_OxygenGenerator), true)]
    public class AbstractTimeLimitedOxygenGeneratorBlock : TimeLimitedBlock
    {
        public override bool HasItemsToProcess()
        {
            var rref = (IMyGasGenerator)m_block;
            if (rref.GetInventory().CurrentVolume <= (MyFixedPoint)0.1)
            {
                return false;
            }
            return true;
        }

        protected override void ApplyBoostMultipler(float v)
        {
            var rref = (IMyGasGenerator)m_block;
            rref.ProductionCapacityMultiplier = v;
            rref.PowerConsumptionMultiplier = v;
        }
    }

    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_WindTurbine), true)]
    public class AbstractTimeLimitedWindTurbineBlock : TimeLimitedBlock { }

    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_SolarPanel), true)]
    public class AbstractTimeLimitedSolarPanelBlock : TimeLimitedBlock { }

    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_OxygenFarm), true)]
    public class AbstractTimeLimitedOxygenFarmBlock : TimeLimitedBlock { }

    public abstract class TimeLimitedBlock : MyGameLogicComponent
    {
        public static Sync<BlockSettingsStorage, TimeLimitedBlock> sync;

        public static HashSet<TimeLimitedBlock> BlocksInViewField = new HashSet<TimeLimitedBlock>();
        protected IMyFunctionalBlock m_block;
        public long BuiltBy = 0;
        public PointsType UsePointsType;
        public BlockConfig m_config;
        private bool m_inited = false;

        private int errorInProduction = 0;
        private int MAX_ERROR_IN_PRODUCTION = 10;

        public BlockSettingsStorage settings = new BlockSettingsStorage();

        public virtual bool HasItemsToProcess() { return true; }
        protected virtual void ApplyBoostMultipler(float v) { }

        public override void Init(MyObjectBuilder_EntityBase ob)
        {
            base.Init(ob);
            NeedsUpdate |= VRage.ModAPI.MyEntityUpdateEnum.BEFORE_NEXT_FRAME;

            if (!Entity.TryGetStorageData(TLBSessionCore.GUID, out settings))
            {
                settings = new BlockSettingsStorage();
            }
        }

        public static void Handler (TimeLimitedBlock block, BlockSettingsStorage settings, ulong SteamUserId, bool isFromServer)
        {
            //MyLog.Default.Error ("Handler: " + block + " " + settings);
            if (isFromServer)
            {
                block.settings = settings;
            } 
            else
            {
                var Identity = SteamUserId.Identity();
                if (Identity == null) {
                    //MyLog.Default.Error("Handler: Identity null");
                    return;
                }
                if (!(block.Entity as IMyFunctionalBlock).HasPlayerAccess(Identity.IdentityId)) {
                    //MyLog.Default.Error("Handler: no access");
                    return;
                }

                //DO Check
                block.settings = settings;
                block.settings.Boost = block.ClampBoost (block.settings.Boost);
                block.SaveSettings();

                block.ApplyBoostMultipler(block.settings.Boost);
                MyAPIGateway.Utilities.SendModMessage(82733000, block.Entity);
                sync.SendMessageToOthers(block.Entity.EntityId, settings);
            }
        }

        public void SaveSettings ()
        {
            (Entity as IMyFunctionalBlock).SetStorageData(TLBSessionCore.GUID, settings);
        }

        public override void UpdateOnceBeforeFrame()
        {
            base.UpdateOnceBeforeFrame();
            //TLBSessionCore.WriteToLog("UpdateOnceBeforeFrame: " + this.GetType());
            if (!m_inited)
            {
                MyInit();
            }
        }

        private void MyInit()
        {
            if (m_inited) return;
            m_inited = true;
            m_block = Entity as IMyFunctionalBlock;
            foreach (var item in TLBSessionCore.CurrentModSettings.TLBlocks)
            {
                try
                {
                    var Matcher = new BlockIdMatcher(item.Matcher);
                    var temp1 = (m_block.SlimBlock is IMySlimBlock);
                    if (temp1)
                    {
                        var temp = (m_block.SlimBlock as IMySlimBlock);
                        if (Matcher.Matches(temp.BlockDefinition))
                        {
                            m_config = item;
                            UsePointsType = item.PointType;
                            //TLBSessionCore.WriteToLog($"Init block UsePointsType {UsePointsType} {m_block.DisplayName} {item.CanBeBoosted} {item.Matcher}");
                            break;
                        }
                    }
                }
                catch { }
            }

            
            BuiltBy = (m_block as MyCubeBlock).BuiltBy;
            m_block.EnabledChanged += Block_EnabledChanged;

            if (!TLBSessionCore.IsServer)
            {
                BlocksInViewField.Add(this);
                sync.RequestData(Entity.EntityId);
            }

            if (!TLBSessionCore.IsDedicated)
            {
                if (!TLBSessionCore.m_controlsCreated)
                {
                    TLBSessionCore.CreateControls();
                }
            }
        }


        private float ClampBoost (float v)
        {
            v = MathHelper.Clamp(v, m_config.MinBoost, m_config.MaxBoost);
            v = ((int)(v * 10)) / 10f;
            return v;
        }

        internal void GUISetBoostMutliplier(float v)
        {
            v = ClampBoost (v);
            if (v == settings.Boost) {
                return;
            }

            settings.Boost = v;
            ApplyBoostMultipler(v);

            if (MyAPIGateway.Session.IsServer)
            {
                MyAPIGateway.Utilities.SendModMessage(82733000, Entity);
                sync.SendMessageToOthers(Entity.EntityId, settings);
                SaveSettings();
            } 
            else
            {
                MyAPIGateway.Utilities.SendModMessage(82733000, Entity);
                sync.SendMessageToServer(Entity.EntityId, settings);
            }
        }

        internal void GUISetAutoTurnOff(bool v)
        {
            if (v == settings.AutoTurnOff) return;

            if (MyAPIGateway.Session.IsServer)
            {
                settings.AutoTurnOff = v;
                sync.SendMessageToOthers(Entity.EntityId, settings);
                SaveSettings();
            }
            else
            {
                settings.AutoTurnOff = v;
                sync.SendMessageToServer(Entity.EntityId, settings);
            }
        }

        public override void OnRemovedFromScene()
        {
            base.OnRemovedFromScene();

            if (!TLBSessionCore.IsServer)
            {
                BlocksInViewField.Remove(this);
            }
        }

        public override void UpdateAfterSimulation100()
        {
            base.UpdateAfterSimulation100();
			if (!MyAPIGateway.Session.IsServer) return;
            try
            {
                if (m_block.Enabled && IsTimeLimitedBlock())
                {
                    var settings = TLBSessionCore.GetPlayerSettings(BuiltBy);
                    var left = settings.Drain(UsePointsType, (int)(m_config.DrainAmount * this.settings.Boost));
                    if (left <= 0)
                    {
                        m_block.Enabled = false;
                    }

                    if (this.settings.AutoTurnOff)
                    {
                        if (!HasItemsToProcess())
                        {
                            errorInProduction++;
                            if (errorInProduction > MAX_ERROR_IN_PRODUCTION)
                            {
                                m_block.Enabled = false;
                                errorInProduction = 0;
                            }
                        }
                        else
                        {
                            errorInProduction = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TLBSessionCore.WriteToLog($"UAFS100 exception:{ex}");
            }
        }

        public bool IsTimeLimitedBlock()
        {
            return m_config != null;
        }

        protected virtual void Block_EnabledChanged(IMyTerminalBlock obj)
        {
            if (!IsTimeLimitedBlock()) return;

            if (m_block.Enabled)
            {
                var settings = TLBSessionCore.GetPlayerSettings(BuiltBy);
                if (settings.Drain(UsePointsType, 0) < m_config.MinimalPointsForActivation)
                {
                    m_block.Enabled = false;
                }
            }
        }
    }
}
