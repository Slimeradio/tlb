using ProtoBuf;
using Sandbox.ModAPI;
using System;
using TLB;
using VRage.Game.Components;
using VRage.ModAPI;

namespace Scripts.Shared
{
    [ProtoContract]
    public class EntitySync
    {

        [ProtoMember(1)] public long entityId = -1;
        [ProtoMember(2)] public byte type = 0; //0 - request, 1 - send
        [ProtoMember(3)] public byte[] data = null;
        public EntitySync() { }
        public EntitySync(long entityId, byte type, byte[] data)
        {
            this.entityId = entityId;
            this.type = type;
            this.data = data;
        }
    }

    public class Connection<T>
    {
        private ushort port;
        private Action<T, ulong, bool> handler;
        public Connection(ushort port, Action<T, ulong, bool> handler)
        {
            this.port = port;
            this.handler = handler;
            MyAPIGateway.Multiplayer.RegisterSecureMessageHandler(port, Handler);
        }

        public void SendMessageToServer(T data, bool reliable = true)
        {
            //Log.ChatError ("SendMessageToServer");
            var bdata = MyAPIGateway.Utilities.SerializeToBinary<T>(data);
            MyAPIGateway.Multiplayer.SendMessageToServer(port, bdata, reliable);
        }

        public void SendMessageToOthers(T data, bool reliable = true)
        {
            var bdata = MyAPIGateway.Utilities.SerializeToBinary<T>(data);
            MyAPIGateway.Multiplayer.SendMessageToOthers(port, bdata, reliable);
        }

        public void SendMessageTo(T data, ulong SteamID, bool reliable = true)
        {
            var bdata = MyAPIGateway.Utilities.SerializeToBinary<T>(data);
            MyAPIGateway.Multiplayer.SendMessageTo(port, bdata, SteamID, reliable);
        }

        public void Handler(ushort HandlerId, byte[] bdata, ulong PlayerSteamId, bool isFromServer)
        {
            //Log.ChatError("Handle message from server: " + PlayerSteamId + " " + isFromServer);
            var data = MyAPIGateway.Utilities.SerializeFromBinary<T>(bdata);
            handler(data, PlayerSteamId, isFromServer);
        }
    }

    public class Sync<T, Z> where Z : MyGameLogicComponent
    {
        public const byte REQUEST = 0;
        public const byte SEND = 1;

        private ushort port;
        private Action<Z, T, ulong, bool> handler;
        private Action<T, ulong, bool> handler2;
        private Func<Z, T> getter;

        public Sync(ushort port, Func<Z, T> getter, Action<Z, T, ulong, bool> handler, Action<T, ulong, bool> handler2 = null)
        {
            this.port = port;
            this.handler = handler;
            this.handler2 = handler2;
            this.getter = getter;
            MyAPIGateway.Multiplayer.RegisterSecureMessageHandler(port, Handler);
        }

        public void Handler(ushort HandlerId, byte[] bdata, ulong PlayerSteamId, bool isFromServer)
        {
            try
            {
                var data = MyAPIGateway.Utilities.SerializeFromBinary<EntitySync>(bdata);
                if (data.type == REQUEST)
                {
                    var x = data.entityId.As<IMyEntity>();
                    if (x != null)
                    {
                        var z = x.GetAs<Z>();
                        if (z != null)
                        {
                            var t = getter.Invoke(z);
                            SendMessageToOthers(data.entityId, t, true);
                        }
                    }

                    return;
                }

                if (data.type == SEND)
                {
                    var x = data.entityId.As<IMyEntity>();
                    if (x != null)
                    {
                        var z = x.GetAs<Z>();
                        if (z != null)
                        {
                            var dataz = MyAPIGateway.Utilities.SerializeFromBinary<T>(data.data);
                            handler.Invoke(z, dataz, PlayerSteamId, isFromServer);
                        }
                    }
                    else
                    {
                        if (handler2 != null)
                        {
                            var dataz = MyAPIGateway.Utilities.SerializeFromBinary<T>(data.data);
                            handler2.Invoke(dataz, PlayerSteamId, isFromServer);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error($"Sync Handler Error! HandlerId:[{HandlerId}] isFromServer[{isFromServer}] ex:{ex} PlayerSteamId:[{PlayerSteamId}]");
                throw;
            }
        }

        public void SendMessageToServer(long entityId, T data, bool reliable = true)
        {
            var bdata = MyAPIGateway.Utilities.SerializeToBinary<EntitySync>(new EntitySync(entityId, SEND, MyAPIGateway.Utilities.SerializeToBinary<T>(data)));
            MyAPIGateway.Multiplayer.SendMessageToServer(port, bdata, reliable);
        }

        public void SendMessageToOthers(long entityId, T data, bool reliable = true)
        {
            var bdata = MyAPIGateway.Utilities.SerializeToBinary<EntitySync>(new EntitySync(entityId, SEND, MyAPIGateway.Utilities.SerializeToBinary<T>(data)));
            MyAPIGateway.Multiplayer.SendMessageToOthers(port, bdata, reliable);
        }

        public void SendMessageTo(long entityId, T data, ulong SteamID, bool reliable = true)
        {
            var bdata = MyAPIGateway.Utilities.SerializeToBinary<EntitySync>(new EntitySync(entityId, SEND, MyAPIGateway.Utilities.SerializeToBinary<T>(data)));
            MyAPIGateway.Multiplayer.SendMessageTo(port, bdata, SteamID, reliable);
        }

        public void RequestData(long entityId, bool reliable = true)
        {
            var bdata = MyAPIGateway.Utilities.SerializeToBinary<EntitySync>(new EntitySync(entityId, REQUEST, null));
            MyAPIGateway.Multiplayer.SendMessageToServer(port, bdata, reliable);
        }
    }
}
